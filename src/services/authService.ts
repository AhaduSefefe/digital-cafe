import { UserToLogin } from "@/models/userModel";

export default class AuthService {
  public static login(user: UserToLogin): string {
    console.log("login from authService with user:", user);
    return "token";
  }

  public static logout(): void {
    console.log("logout from authService");
  }
}