import { UserToLogin } from "@/models/userModel";
import { defineStore } from "pinia";
import  AuthService  from "@/services/authService";
export const useAuthStore = defineStore("authStore", {
  state: () => ({
    auth: {
      isLoggedIn: false,
      user: { username: "", password: "" } as UserToLogin,
      token: "",
    },
  }),
  getters: {
    isLoggedIn: (state) => state.auth.isLoggedIn,
    user: (state) => state.auth.user,
    token: (state) => state.auth.token,
  },
  actions: {
    login(user: UserToLogin) {
      const token = AuthService.login(user);
      console.log("token from authService:", token);
      //check if isLoggedIn
      if (token) {
        this.auth.isLoggedIn = true;
        this.auth.user = user;
        this.auth.token = token;
        console.log("login from authStore with user:", user);
      }
    },
    logout() {
      console.log("logout from authStore");
      AuthService.logout();
      this.auth.isLoggedIn = false;
      this.auth.user = { username: "", password: "" } as UserToLogin;
      this.auth.token = "";
    },
  },
});
